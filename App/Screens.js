import React, { useEffect, useState } from "react";

import { View, Text, StyleSheet, Button, Image } from "react-native";
import { TextInput } from "react-native-paper";

import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5,
  },
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Home = ({ navigation }) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("http://192.168.1.16:8000/webservice.php")
      .then((response) => response.json())
      .then((data) => setProducts(data))
      .catch((error) => console.log(error));
  });

  return (
    <ScreenContainer>
      <Text style={{ fontWeight: "bold", fontSize: 12, padding: 50 }}>
        List of Products : {JSON.stringify(products)}
      </Text>

      <Button title="Menu" onPress={() => navigation.toggleDrawer()} />
    </ScreenContainer>
  );
};

export const Details = ({ route }) => (
  <ScreenContainer>
    <Text>Details Screen</Text>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text style={{ fontWeight: "bold", fontSize: 50, padding: 50 }}>
        Welcome in our university :
      </Text>
      <Text style={{ fontSize: 20, padding: 15 }}>
        University Institute of Technology in Menzel Abderrahmane Address: Route
        Menzel Abderrahmen - Zarzouna 7021 Bizerte ، 7021 The IT department was
        created at the start of the 2006/2007 school year. The training offered
        is spread over three years, choosing a specialty among the following:
        Development of information systems Networks and IT services Embedded and
        mobile systems
      </Text>
      <Button title="Sign Out" onPress={() => signOut()} />
    </ScreenContainer>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Loading...</Text>
  </ScreenContainer>
);

export const SignIn = ({ navigation }) => {
  const { signIn } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text style={{ fontWeight: "bold", fontSize: 50 }}>Sign In Here </Text>
      <Text style={{ fontSize: 30 }}> Email : </Text>
      <TextInput
        placeholder={"Your email"}
        style={{ width: "80%", height: 42, borderBottomWidth: 1 }}
      />

      <Text style={{ fontSize: 30 }}> Password : </Text>
      <TextInput
        placeholder={"Your Password"}
        style={{ width: "80%", height: 42, borderBottomWidth: 1 }}
      />
      <Button title="Sign In" onPress={() => signIn()} />
    </ScreenContainer>
  );
};

export const CreateAccount = () => {
  const { signUp } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text>Create Account Screen</Text>
      <Button title="Sign Up" onPress={() => signUp()} />
    </ScreenContainer>
  );
};
